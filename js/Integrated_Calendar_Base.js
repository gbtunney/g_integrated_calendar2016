/**
 * Created with JetBrains PhpStorm.
 * User: gbtunney
 * Date: 8/6/14
 * Time: 8:17 PM
 * To change this template use File | Settings | File Templates.
 *
 CALENDAR DEFAULT
 IntegratedCalendar_GoogleCalendar_Path[ field ]  Integrated_Calendar_text_field_0
 IntegratedCalendar_Calendar_Width [ field ] integrated_calendar_text_field_1
 IntegratedCalendar_Calendar_Height[ field ]  integrated_calendar_text_field_2
 IntegratedCalendar_ShortCode_Enabled integrated_calendar_checkbox_field_3


 WIDGET
 IntegratedCalendar_Widget_Enabled
 IntegratedCalendar_CookieLoad_Enabled

 SINGLE EVENT
 IntegratedCalendar_SingleEventPopup_Enabled
 IntegratedCalendar_SingleEventTooltips_Enabled
 IntegratedCalendar_Tooltip_Width[ field ] integrated_calendar_text_field_8
 <div class="panel-group" id="accordion">
 <div class="panel panel-default">
 <div class="panel-heading">
 <h4 class="panel-title">
 <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
 Collapsible Group Item #1
 </a>
 </h4>
 </div>
 <div id="collapseOne" class="panel-collapse collapse in">
 <div class="panel-body">
 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
 </div>
 </div>
 </div>
 <div class="panel panel-default">
 <div class="panel-heading">
 <h4 class="panel-title">
 <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
 Collapsible Group Item #2
 </a>
 </h4>
 </div>
 <div id="collapseTwo" class="panel-collapse collapse">
 <div class="panel-body">
 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
 </div>
 </div>
 </div>
 <div class="panel panel-default">
 <div class="panel-heading">
 <h4 class="panel-title">
 <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
 Collapsible Group Item #3
 </a>
 </h4>
 </div>
 <div id="collapseThree" class="panel-collapse collapse">
 <div class="panel-body">
 Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
 </div>
 </div>
 </div>
 </div>

 */

/// CALENDAR DEFAULT

var IntegratedCalendar_GoogleCalendar_User = IntegratedCalendar_Vars.IntegratedCalendar_GoogleCalendar_User;
var IntegratedCalendar_Calendar_Width = IntegratedCalendar_Vars.IntegratedCalendar_Calendar_Width;
var IntegratedCalendar_Calendar_Height = IntegratedCalendar_Vars.IntegratedCalendar_Calendar_Height;
var IntegratedCalendar_ShortCode_Enabled = IntegratedCalendar_Vars.IntegratedCalendar_ShortCode_Enabled;

// WIDGET
var IntegratedCalendar_Widget_Enabled = IntegratedCalendar_Vars.IntegratedCalendar_Widget_Enabled;
var IntegratedCalendar_CookieLoad_Enabled = IntegratedCalendar_Vars.IntegratedCalendar_CookieLoad_Enabled;  //not using this for now.
var IntegratedCalendar_Widget_Result_Limit = IntegratedCalendar_Vars.IntegratedCalendar_Widget_Result_Limit;
// SINGLE EVENT
var IntegratedCalendar_SingleEventPopup_Enabled = IntegratedCalendar_Vars.IntegratedCalendar_SingleEventPopup_Enabled;
var IntegratedCalendar_SingleEventTooltips_Enabled = IntegratedCalendar_Vars.IntegratedCalendar_SingleEventTooltips_Enabled;
var IntegratedCalendar_Tooltip_Width = IntegratedCalendar_Vars.IntegratedCalendar_Tooltip_Width;


var TEMPLATES = IntegratedCalendar_Vars.TEMPLATES;
var HOME_SITE = IntegratedCalendar_Vars.HOME_SITE;
var CURRENT_URI = IntegratedCalendar_Vars.CURRENT_URI;
var PLUGIN_NAME_PATH =  IntegratedCalendar_Vars.PLUGIN_NAME_PATH

var SINGLE_EVENT_URL = null;
var GOOGLE_CALENDAR_API_KEY = IntegratedCalendar_Vars.GOOGLE_CALENDAR_API_KEY;


var tooltipItemsArr = [];
if (IntegratedCalendar_Vars.SINGLE_EVENT_URL) {

    SINGLE_EVENT_URL = IntegratedCalendar_Vars.SINGLE_EVENT_URL;

}

var addlParamsWidget ={
    'max-results':IntegratedCalendar_Widget_Result_Limit,
    futureevents: true,
    orderby: 'starttime',
    sortorder:'ascending',
    singleEvents: true,



};

var addlParams ={
    singleEvents: true,

};



jQuery(document).ready(function ($) {


    console.log("INITING APP");






    ////create shortcode  "https://www.google.com/calendar/feeds/fmueefdsaok7rogdtp5g2bqjsk%40group.calendar.google.com/public/basic"
    var ShortcodeCalObj = new Integrated_Calendar_Object(GOOGLE_CALENDAR_API_KEY,"shortcodecal",
        null,
        '#integrated-calendar',
        'main-cal-event',addlParams, false,
        IntegratedCalendar_ShortCode_Enabled, null, addTooltip,null);


    //create single event
    if (singleEventMode()) {


        jQuery("body").append("<div id='integrated_calendar_single_eventobj' ></div>");


        var SingleEventCalObj = new Integrated_Calendar_Object(GOOGLE_CALENDAR_API_KEY,"single_event", SINGLE_EVENT_URL,
            '#integrated_calendar_single_eventobj', 'main-cal-event',
            addlParams,
            true, IntegratedCalendar_SingleEventPopup_Enabled, createSingleView,null);
    }
    //*** CREATE WIDGET***//

    if (IntegratedCalendar_Widget_Enabled) {

        jQuery("body").append("<div id='integrated_calendar_widget_render' ></div>");
    }

    var widgetLoaderView = new Integrated_Calendar_View("integrated_calendar_widget", {},
        "#integrated_calendar_widget", TEMPLATES["loader"], IntegratedCalendar_Widget_Enabled,null,true,true);
    var WidgetCalObj = new Integrated_Calendar_Object(GOOGLE_CALENDAR_API_KEY,"widgetCal",null/*"e582008218599362@facebook.com"*/,
        '#integrated_calendar_widget_render', 'widget-cal-event',addlParamsWidget,
        true, IntegratedCalendar_Widget_Enabled, createWidgetView,null,true);


});


var singleEventMode = function () {


    if (SINGLE_EVENT_URL && IntegratedCalendar_SingleEventPopup_Enabled) {

        return true;
    } else {

        return false;
    }

}



var openSingleEvent = function (event) {


    renderTemplateEvent( event); //for my cheating



    hideAllTooltips();
    jQuery('#integrated_calendar_single_render').remove(); // clear the previous ids.

    jQuery("body").append("<div id='integrated_calendar_single_render' >wasdsad</div>");

    var singleEvent = new Integrated_Calendar_View("integrated_calendar_singleevent", [event], "#integrated_calendar_single_render",
        TEMPLATES["singleevent"], IntegratedCalendar_SingleEventPopup_Enabled, null, true, true);

console.log("rend!" + singleEvent.getContent());

    jQuery('.mfp-content').addClass('mfp-content-mobile');

    if( isMobile.any() ){

        var zoom = getZoomLevel();

        var vis= getVisibleArea() ;
   //   alert ("visible ar " + vis['width'] + " x " + window.pageXOffset +  " calc " + (vis['width'] *.9));

 jQuery ( '#integrated_calendar_single_render').css("width", (vis['width'] *.9)+ 'px' );


        jQuery.magnificPopup.open({
        items: {
            src: '#integrated_calendar_single_render',
// can be a HTML string, jQuery object, or CSS selector
            type: 'inline'
        },
        alignTop: true,/*mainClass: 'mfp-content-mobile'*/
    });
        jQuery('.mfp-inline-holder').width(0);
  jQuery('.mfp-content').css("position",'absolute' );
       jQuery('.mfp-content').css("margin",'0' );
       jQuery('.mfp-content').css("left",(window.pageXOffset+ (.5*(vis['width'] *.1)))+"px" );
/*

    .mfp-content {
            position: relative;
            // display: inline-block;
            vertical-align: middle;
            margin: 0 auto;
            text-align: left;
            z-index: 1045;*/


//background-color: #ff0000;
       // }

    }else{

        jQuery.magnificPopup.open({
            items: {
                src: '#integrated_calendar_single_render',
// can be a HTML string, jQuery object, or CSS selector
                type: 'inline'
            },

        });


    }


    /*jQuery('.test-popup-link').magnificPopup({
        type: 'image',
        alignTop:true,fixedContentPos:true,
        callbacks: {
            open: function() {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object



                jQuery('img.mfp-img').css('max-width',  window.innerWidth+'px');

                alert("Sett"+window.innerWidth);

            },
            close: function() {
                // Will fire when popup is closed
            }
            // e.t.c.
        }


    });*/

  /*  jQuery('#myModal').reveal({
        animation:'fadeAndPop', //fade, fadeAndPop, none
        animationspeed:300, //how fast animtions are
        closeonbackgroundclick:true, //if you click background will modal close?
        dismissmodalclass:'close-reveal-modal'    //the class of a button or element that will close an open modal
    });
*/
}

var createSingleView = function (events) {


    if (events.length == 1) {
        openSingleEvent(events[0]);
    }

}


var widgetCallbackFunction = function (event) {
    openSingleEvent(event.data.eventobj);

}
var showEventStr = function (str) {

    alert("HI" + str);
}
var createWidgetView = function (events) {
//TODO: Need to move this soon.


  //  console.log("THE LENGTH IS" +events.length);
    if ( !events ){

        var widgetView = new Integrated_Calendar_View("integrated_calendar_widget", [],
            "#integrated_calendar_widget", TEMPLATES["noevents"], IntegratedCalendar_Widget_Enabled,null,true,true);



    }else{

        bindObj(events);



    }



}

function bindObj(events){

    var bindableObj = { callbackEvent:"click", callbackFunc:widgetCallbackFunction};

    var widgetView = new Integrated_Calendar_View("integrated_calendar_widget", events,
        "#integrated_calendar_widget", TEMPLATES["widget"],
        IntegratedCalendar_Widget_Enabled, bindableObj,true,true);
    var myEvents = widgetView.getEvents();


    if ( myEvents.length > 0){
         for (var u =0; u< myEvents.length;u++) {
         jQuery("." + myEvents[u].viewClass).on("click", {eventobj:myEvents[u], viewname:"test"}, widgetCallbackFunction)
          }
    }
}

/* override bootstrap popover to include callback */

//var tooltipManager;

jQuery(document).on('shown.bs.tooltip', function (ev) {
    var myarr = tooltipItemsArr;
    var $target = jQuery(ev.target);

    for (var i = 0; i < tooltipItemsArr.length; i++) {

        if ($target.is(jQuery(tooltipItemsArr[i]))) {

        } else {

            jQuery(tooltipItemsArr[i]).tooltip().tooltip('hide');
        }
    }

    //   if ($currentPopover && ($currentPopover.get(0) != $target.get(0))) {
    //    $currentPopover.tooltip('toggle');
    // }
    //  $currentPopover = $target;
});

/*

 jQuery(document).on('hidden.bs.tooltip', function (ev) {
 var $target = jQuery(ev.target);
 if ($currentPopover && ($currentPopover.get(0) == $target.get(0))) {
 $currentPopover = null;
 }
 });


 */

var hideAllTooltips = function () {

    if (tooltipItemsArr.length > 0) {
        for (var i = 0; i < tooltipItemsArr.length; i++) {

            jQuery(tooltipItemsArr[i]).tooltip().tooltip('hide');


        }

    }
}
var addTooltip = function (events) {


    tooltipItemsArr = [];


////CAN REMOVE THIS EVENTUALLY BUT LEAVING FOR NOW.
    var showPopover = jQuery.fn.tooltip.Constructor.prototype.show;
    jQuery.fn.tooltip.Constructor.prototype.show = function () {
        showPopover.call(this);
        if (this.options.showCallback) {
            this.options.showCallback.call(this);
        }
    }

    var hidePopover = jQuery.fn.tooltip.Constructor.prototype.hide;
    jQuery.fn.tooltip.Constructor.prototype.hide = function () {
        if (this.options.hideCallback) {
            this.options.hideCallback.call(this);
        }
        hidePopover.call(this);
    }


    if (IntegratedCalendar_SingleEventTooltips_Enabled) {
        for (var i = 0; i < events.length; i++) {
            var templateRend = TemplateEngine(TEMPLATES["tooltip"], {events:[ events[i]]});
            jQuery('.tooltip-' + events[i].uuid).tooltip({
                container:'body',
                trigger:'hover',
                delay:{
                    show:"50",
                    hide:"1000"
                }, showCallback:function () {
                    console.log('popover is shown');

                    //     jQuery('.fc-event').not(this).tooltip('hide');
                    //  tooltipHover(this);
                },
                hideCallback:function () {
                    console.log('popover is hidden');
                },
                animation:false,
                placement:'left',
                title:templateRend,
                html:true});

            tooltipItemsArr.push(jQuery('.tooltip-' + events[i].uuid));
        }
    }

    //  tooltipManager.addTooltips(tooltipItemsArr);



}


///// g added utilities

var getFBEventID = function (event) {


    var eventIDStr = event.iCalUID;


    if (eventIDStr.charAt(0) == "e") {

        eventIDStr = eventIDStr.substring(1)
    }
    var tempArr = eventIDStr.split("@");

    return tempArr[0];


}


var renderTemplateEvent = function ( event) {

    var templateid=  "postevent";

    if ( TEMPLATES[templateid] ){
        var template =TEMPLATES[templateid];


        var dataObj =   {events: [event],
            title:'Gillian Tunney',
            'PLUGIN_NAME_PATH': PLUGIN_NAME_PATH,
            'HOME_SITE': HOME_SITE,
        }

        // jQuery( this.holderid)


        var templateRend =   TemplateEngine(template,dataObj);

        console.log("TEMPLATE "+ templateid + " RENDERED: " + templateRend);
    }else{
        // console.log("NO CHEATING" );


    }

}


/////**** UTIlITIES
function validateURL(value) {
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
}
function updateURLParameter(url, param, paramVal) {
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] != param) {
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}


jQuery.fn.exists = function (callback) {
    var args = [].slice.call(arguments, 1);

    if (this.length) {
        callback.call(this, args);
    }

    return this;
};
var short_guid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return function () {
        return s4() + s4() + '' + s4() + '' + s4();
    };
})();
var guid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
})();

var TemplateEngine = function (html, options) {

    var re = /<%([^%>]+)?%>/g, reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g, code = 'var r=[];\n', cursor = 0;
    var add = function (line, js) {
        js ? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
            (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
        return add;
    }
    while (match = re.exec(html)) {
        add(html.slice(cursor, match.index))(match[1], true);
        cursor = match.index + match[0].length;
    }
    add(html.substr(cursor, html.length - cursor));
    code += 'return r.join("");';
    return new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
    return new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
}

/*   example:: var template =
 'My skills:' +
 '<%if(this.showSkills) {%>' +
 '<%for(var index in this.skills) {%>' +
 '<a href="#"><%this.skills[index]%></a>' +
 '<%}%>' +
 '<%} else {%>' +
 '<p>none</p>' +
 '<%}%>';
 console.log(TemplateEngine(template, {
 skills: ["js", "html", "css"],
 showSkills: true
 }));
 */
var shortenText = function (text, maxLength) {
    if (!text){

        text="   ";
    }
    var ret = text;
    if (ret.length > maxLength) {
        ret = ret.substr(0, maxLength - 3) + "...";
    }
    return ret;
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};