/**
 * Created with JetBrains PhpStorm.
 * User: gbtunney
 * Date: 8/6/14
 * Time: 8:11 PM
 * To change this template use File | Settings | File Templates.
 */
function Integrated_Calendar_View(_id,_events,_holderid, _template ,_enabled, _bindableObj, _addToStage , _clearHolder ) {


    this.id = _id;
this.events = _events;
   this.holderid = _holderid;
    this.template = _template;
this.enabled=_enabled;

this.addToStage = _addToStage;
this.clearHolder = _clearHolder; /// clear container
this.content = null;


    this.bindableObj= _bindableObj;   //{ callbackEvent: , callbackFunc:

    console.log("creating " + this.events.length );


    ///process events

    this.eventsArr = [];


    for (var i=0; i<this.events.length ; i++){


        this.events[i]["viewClass"] = _id +"-" + this.events[i].uuid;
        this.eventsArr.push( this.events[i]);


    }


    if ( this.checkStatus()  ){
        this.initView();

        if (_bindableObj){
                //this.makeBindable();
        }
    }else{


        console.log("Status check failed");

    }



}Integrated_Calendar_View.prototype.getEvents = function(){

    return this.eventsArr;
}

    Integrated_Calendar_View.prototype.getContent = function(){
return this.content;
}
    Integrated_Calendar_View.prototype.makeBindable = function(){

    var _self = this;

    function handlerName(event)
    {

       // console.log("HANDLED " + (event.data.eventobj + "  "+event.data.viewname));

    //    _self.bindableObj(event);//
        _self.bindableObj["callbackFunc"](event);
    }

        /*
    for (var i in this.events)
    {


        console.log("THE EVENT IS BINDABLE AND IS " + this.events[i].uuid);

       jQuery("." + this.events[i].uuid).on(this.bindableObj["callbackEvent"], {eventobj: this.events[i],viewname: this.id}, handlerName)


    }*/

}
Integrated_Calendar_View.prototype.initView = function(){

if ( this.clearHolder){   /// please fix

    jQuery( this.holderid).empty();

  var dataObj =   {events: this.events,
      title:'Gillian Tunney',
      'PLUGIN_NAME_PATH': PLUGIN_NAME_PATH,
      'HOME_SITE': HOME_SITE,
  }

   // jQuery( this.holderid)
}

    var templateRend =   TemplateEngine(this.template,dataObj);

this.content =templateRend;


  if ( this.addToStage){

        jQuery( this.holderid).append(templateRend);

  }




}
Integrated_Calendar_View.prototype.checkStatus=function(){
    var holderexcists = false;

    if ( !this.addToStage ){
        holderexcists=true;

    }else{
    jQuery(this.holderid).exists(function() {
        holderexcists= true;



    });
    }
    if ( holderexcists && this.enabled){


        return true;
    }else{

        return false;
    }
}