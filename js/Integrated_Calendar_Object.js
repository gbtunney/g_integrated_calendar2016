/**
 * Created with JetBrains PhpStorm.
 * User: gbtunney
 * Date: 8/6/14
 * Time: 8:13 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains PhpStorm.
 * User: gbtunney
 * Date: 8/6/14
 * Time: 8:11 PM
 * To change this template use File | Settings | File Templates.
 */

function Integrated_Calendar_Object(_apikey, _id, _calendarEventID, _holderid, _calEventClassName, _params, _unrendered, _enabled , _loadedCallback , _eventRenderCallback,_futureEventsOnly) {

this.apikey = _apikey;
    this.id = _id;
    this.calendarEventID = _calendarEventID;  //if path its a single event.
    this.holderid = _holderid;
    this.calEventClassName = _calEventClassName;
    this.unrendered = _unrendered;
    this.enabled = _enabled;
    this.params = _params;

if ( _futureEventsOnly){
this.futureEventsOnly = _futureEventsOnly;}else{

    this.futureEventsOnly=false;
}

    this.loadedCallback = _loadedCallback;
    this.eventRenderCallback = _eventRenderCallback; // dont use this anymore

    if (this.checkStatus()) {
        this.init_Calendar();
    } else {
    }
}
Integrated_Calendar_Object.prototype.checkStatus = function () {
    var holderexcists = false;

    jQuery(this.holderid).exists(function () {
        holderexcists = true;
    });

    if (holderexcists && this.enabled) {
        return true;
    } else {
        return false;
    }
}

Integrated_Calendar_Object.prototype.process_Events = function (events) {

    var myEvents = events;

    for (var i in myEvents) {


        var ms = moment(myEvents[i].end, "DD/MM/YYYY HH:mm:ss").diff(moment(myEvents[i].start, "DD/MM/YYYY HH:mm:ss"));
        var d = moment.duration(ms).as('hours');
        if (d == 24) {

            myEvents[i].isAllDayEvent = moment(myEvents[i].start).format("dddd, MMMM Do YYYY")
            myEvents[i].AllDayEventWidget = moment(myEvents[i].start).format("ddd, MMM Do");

        }
        ///myEvents[i].widgetDate = moment(myEvents[i].start).format("dddd, MMMM Do");

        myEvents[i].widgetDate = moment(myEvents[i].start).format("ddd, MMM Do");

        myEvents[i].startlong = moment(myEvents[i].start).format("dddd, MMMM Do YYYY@ h:mm a");
        myEvents[i].endlong = moment(myEvents[i].end).format("dddd, MMMM Do YYYY@ h:mm a");


    }

    return myEvents;
}


Integrated_Calendar_Object.prototype.init_Calendar = function () {

    var _self = this;


   // updateURLParameter(url, "eventID", value);









    var addslashes =function(str) {
        return (str+'').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
    }


    var isFacebook = function (event) {


        var patt = /facebook/g;


        return  patt.test(event);


    }

    var getPermalink = function (url, value) {
        return  updateURLParameter(url, "eventID", value);
    }

    jQuery(this.holderid).fullCalendar({

        googleCalendarApiKey: this.apikey ,
        eventClick:function (event) {
            openSingleEvent(event);
            if (event.url) {
                //  window.open(event.url);
                return false;
            }
        },
        selectable:false,

        selectHelper:false,

        editable:false,

        norender:_self.unrendered,

        teststr:_self.holderid,

        eventRender:function (event, element, view) {
        },

        header:{
            left:'prev,next today',
            center:'title',
            right:'month,basicWeek,basicDay'
        },
        events:{

            googleCalendarId:IntegratedCalendar_GoogleCalendar_User,
            className:this.calEventClassName,
            iCalUID:this.calendarEventID,

            addlParams: this.params,
            futureEventsOnly : this.futureEventsOnly
        },

        myreportEvents:function (cal, events, rendered) {
            if (_self.unrendered) {
                jQuery(_self.holderid).fullCalendar('destroy');
            }

            if (_self.loadedCallback) {
                    if ( events.length > 0){
                _self.loadedCallback(_self.process_Events(events));
                    }else{
                        _self.loadedCallback( null );
                    }
            }

        },
        loading:function (bool) {
            if (bool) {

                jQuery(_self.holderid).css({"visibility":"hidden"});
                //alert('Rendering events');
            } else {

                if (!_self.unrendered) {
                    jQuery(_self.holderid).css({"visibility":"visible"});
                }
            }
        },
        eventDataTransform:function (eventData) {
            /*  return {
             id: rawEventData.id,
             title: rawEventData.title,
             start: rawEventData.start,
             end: rawEventData.end,
             url: rawEventData.url
             };*/

            var myEventLink = "";

            if (isFacebook(eventData.iCalUID)) {
                eventData.isFacebook = true;
                eventData.facebookImageUrl = getFBEventID(eventData);
                myEventLink ="e"+ getFBEventID(eventData)+"@facebook.com";
            }else{
               myEventLink =  eventData.id+"@google.com";


            }


            eventData.permalink = getPermalink(HOME_SITE, encodeURIComponent(myEventLink));
            eventData.locallink = getPermalink(CURRENT_URI, encodeURIComponent(eventData.eventFeedURL));


            // '{}'
            eventData.shortDesc = shortenText(eventData.description, 200);  // abbreviated description
            eventData.uuid = short_guid();
            eventData.className = 'tooltip-' + eventData.uuid;
            eventData.objstring = JSON.stringify(eventData);


            return eventData;     //add the uid and face book data here.

        },
        eventAfterAllRender:function (view, events) {


            if (_self.eventRenderCallback) {
                _self.eventRenderCallback(_self.process_Events(events));
            }
        }
    });
}

