
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function getZoomLevel(){
    if (  isMobile.any() ){

        var zoomLevel = document.documentElement.clientWidth / window.innerWidth;
        return zoomLevel;

    }else{

        return null;
    }

}

function getVisibleArea(){
    if (  isMobile.any() ){

        return { width:window.innerWidth,
        height: window.innerHeight
        };



    }else{

        return null;
    }

}


/*

var zoomLevel = document.documentElement.clientWidth / window.innerWidth;


var height = window.innerHeight * zoomLevel;*/
