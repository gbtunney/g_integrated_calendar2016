/**
 * Created with JetBrains PhpStorm.
 * User: gbtunney
 * Date: 8/5/14
 * Time: 4:34 PM
 * To change this template use File | Settings | File Templates.
 */
var calendarEvents;


jQuery( document ).ready(function( $ ) {
    // Code using $ as usual goes here.
    //alert("here:"+ cal_script_vars.url);
   makeCalendar(cal_script_vars.url,false)

});

function makeCalendar(path, render ){

    jQuery('#integrated-calendar').fullCalendar({
        events: path,
        norender:render,
        eventRender: function(event, element) {
            //  element.attr("myownlink",'test')
        }
        ,loading: function (bool) {
            if (bool) {
                jQuery("#integrated-calendar").css({"visibility": "hidden"});
                //alert('Rendering events');
            } else {

                if (render){
                jQuery("#integrated-calendar").css({"visibility": "visible"});
                }
                //alert('Rendered and ready to go');
            }
        },

        myreportEvents: function(cal,events,rendered) {
            calendarEvents=events;
        renderTemplateWidget(events);


if (!render){
             jQuery('#integrated-calendar').fullCalendar( 'destroy' );}


        },eventDataTransform :function( eventData ) {

            /*  return {
             id: rawEventData.id,
             title: rawEventData.title,
             start: rawEventData.start,
             end: rawEventData.end,
             url: rawEventData.url
             };*/


      //     eventData.jURL=  "javascript:showEvent('"+objToString(eventData)+"')";





                eventData.isFacebook =    isFacebook(eventData.id);
            eventData.uuid = guid();
          console.log(eventData.id+"IS FACEBOOK" +objToString(eventData) )
            return eventData;      //add the uid and face book data here.

        },

        eventAfterAllRender: function(view){

          //  console.log("RENDERED" + $(this ));
        //    setCalSource(jQuery(this ));
        }
    });

}



var renderTemplateWidget= function(events){
    var template= getWidgetTemplate();

   console.log("temp"+ template);
    var templateRend =   TemplateEngine(template,{events: events, title:'Gillian Tunney'});
    jQuery('.integrated_calendar_widget').exists(function() {
        this.append(templateRend);
    });


    jQuery('.int_item').bind( "click", {
        obj: "HIHIHHI"
    }, function( event ) {
      //  alert( event.data.obj );
       //alert( );
        showEvent(jQuery(this).attr('uuid'));
    });


}
var guid = (function() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return function() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
})();


function  isFacebook( event){


    var patt=/facebook/g;


    return  patt.test(event);



}
var TemplateEngine = function(html, options) {

    var re = /<%([^%>]+)?%>/g, reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g, code = 'var r=[];\n', cursor = 0;
    var add = function(line, js) {
        js? (code += line.match(reExp) ? line + '\n' : 'r.push(' + line + ');\n') :
            (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
        return add;
    }
    while(match = re.exec(html)) {
        add(html.slice(cursor, match.index))(match[1], true);
        cursor = match.index + match[0].length;
    }
    add(html.substr(cursor, html.length - cursor));
    code += 'return r.join("");';
    return new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
}


var getPopupTemplate= function(){

var temp='<div id="wrapperDiv" style="width: 595px;">\
  <div class="tt_Title" style="font-size: 18px;"><%this.title%></div>\
  <hr class="ttRule" style="background-color: rgb(88, 87, 86); height: 1px; width: 100%; margin-right: 3px; margin-bottom: 7px; padding: 0px; border-width: 0px;">\
  <div style="width: 100%;"><strong>Location: </strong><%this.location%></div>\
  <div><strong>Time: </strong><%this.start%> - <%this.end%></div>\
  <a href="http://pyromancy.org?p=9&amp;eventID=<%this.id%>" style="float: right;">\
  <div>Permalink</div>\
  </a>\
  <div class="eventLinkWrapper"><img src="http://pyromancy.org/wp-content/plugins/g-cal/images/fbicon.png"><a href="<%this.url%>">\
    <div>Facebook Event Link\
      <div></div>\
    </div>\
    </a></div>\
  <img src="http://graph.facebook.com/620546754723314/picture?type=large" style="float: left; margin: 4px 4px -10px;">\
  <div style="width: 100%; white-space: pre-wrap;"><%this.description%></div>\
</div>';
    return temp;

}



var getWidgetTemplate= function(){


  var widg=   '<div style="text-align: left;"><a href="<%this.url%>">'
+ '<div><strong><%this.start%> :</strong><%this.title%></div>'
       + '</a></div>';

var widg='<div id="g_cal_upcoming_widget">\
  <%this.title%>   <%for(var index in this.events) {%>\
  <div class="int_item" uuid="<%this.events[index].uuid%>"><a href="#">\
    <div><strong>\
      <%this.events[index].start%>\
      :</strong>\
      <%this.events[index].title%>\
    </div>\
    </a></div>\
  <%}%>\
</div>';

return widg;
}


jQuery.fn.exists = function(callback) {
    var args = [].slice.call(arguments, 1);

    if (this.length) {
        callback.call(this, args);
    }

    return this;
};

function objToString (obj) {
    var tabjson=[];
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            tabjson.push('"'+p +'"'+ ':' + obj[p]);
        }
    }  tabjson.push()
    return '{'+tabjson.join(',')+'}';
}
function matchuuid(uuid){
    for (var i=0 ; i< calendarEvents.length ; i++ ) {
    if ( calendarEvents[i]['uuid']==uuid){


        return calendarEvents[i];
    }
    }
}
function showEvent(uuid){

   var event=  matchuuid(uuid);
    console.log("temp"+ event);


   var template= getPopupTemplate();

    var templateRend =   TemplateEngine(template,event);
    //jQuery( '#int_cal_holder').append(templateRend);


    jQuery("#int_cal_holder").append( "<div id='g_cal_single_event'/>");






    jQuery("#g_cal_single_event").append( templateRend);

    //jQuery( "#dialog" ).dialog();





    jQuery( "#g_cal_single_event" ).dialog({

        title: null,
        modal: true,
        resizable: false,
        width: 400,

        maxHeight:400,

        draggable: true,
        show: 'fade',
        hide: 'fade',






    });





    // alert ("SHOW 0" + event);  // int_cal_holder
}