<?php
/*
Plugin Name: G Integrated Calendar
Plugin URI: http://pyromancy.org
Description: Plugin is counting bots visits
Author: Greg
Version: 1.0
Author URI: http://ditio.net
*/

/* IN CASE NEED TO DO SESSION VARS IN FUTURE.
$testjson = file_get_contents($GLOBALS["PLUGIN_NAME_PATH"].'/templates/testsession.html');

var_dump(json_decode($testjson, true));
*/

/*
CALENDAR DEFAULT
IntegratedCalendar_GoogleCalendar_Path[ field ]  Integrated_Calendar_text_field_0
IntegratedCalendar_Calendar_Width [ field ] integrated_calendar_text_field_1
IntegratedCalendar_Calendar_Height[ field ]  integrated_calendar_text_field_2
IntegratedCalendar_ShortCode_Enabled integrated_calendar_checkbox_field_3


WIDGET
IntegratedCalendar_Widget_Enabled
IntegratedCalendar_CookieLoad_Enabled

SINGLE EVENT
IntegratedCalendar_SingleEventPopup_Enabled
IntegratedCalendar_SingleEventTooltips_Enabled
IntegratedCalendar_Tooltip_Width[ field ] integrated_calendar_text_field_8

*/
////TODO: CLICK EVENT FOR WIDGET.  HOVER EVENT FOR CALENDAR 
////TESTING GILLIAN
require_once 'Mobile-Detect-2.8.3/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();



require_once("Integrated_Calendar_Widget.php");
//require_once("Integrated_Calendar_Options.php");

//*********GLOBAL VARS AND SETTINGS *********//
$GLOBALS["PLUGIN_NAME_PATH"] = plugins_url('', __FILE__);
$GLOBALS["OPTION_DATA_ID"] = "Integrated_Calendar_Data";
$GLOBALS["IntegratedCalendarOptions"];
$GLOBALS["MOBILE"] = false;




if ( $detect->isMobile() ) {
   // echo "this is a mobile";

    $GLOBALS["MOBILE"] =true;
}else{

  //  echo "this is not mobile";
}


//*********INIT TEMPLATES*********//
$GLOBALS["TEMPLATES"] = array();
$GLOBALS["TEMPLATES"]["widget"] = file_get_contents($GLOBALS["PLUGIN_NAME_PATH"] . '/templates/widget.html');
$GLOBALS["TEMPLATES"]["singleevent"] = file_get_contents($GLOBALS["PLUGIN_NAME_PATH"] . '/templates/singleevent.html');
$GLOBALS["TEMPLATES"]["tooltip"] = file_get_contents($GLOBALS["PLUGIN_NAME_PATH"] . '/templates/tooltip.html');
$GLOBALS["TEMPLATES"]["loader"] = file_get_contents($GLOBALS["PLUGIN_NAME_PATH"] . '/templates/loader.html');
$GLOBALS["TEMPLATES"]["noevents"] = file_get_contents($GLOBALS["PLUGIN_NAME_PATH"] . '/templates/noevents.html');



//for my cheating
$templateRenderingWeb = true;
if ($templateRenderingWeb){
$GLOBALS["TEMPLATES"]["postevent"] = file_get_contents($GLOBALS["PLUGIN_NAME_PATH"] . '/templates/postevent.html');

}

$eventID = null;

if (isset ($_GET["eventID"])) {
    $eventID = rawurldecode($_GET["eventID"]);
    ///check to see if its valid.
    if (filter_var($eventID, FILTER_VALIDATE_EMAIL) === FALSE) {
     //   die('Not a valid URL');
       $eventID = null;
    }
}
if ($eventID) {
    $GLOBALS["SINGLE_EVENT_URL"] = $eventID;
}




//*********WORDPRESS HOOKS*********//
add_action('init', 'int_cal_init');
add_shortcode('integratedCalendar', 'my_shortcode_handler_intcal');
add_action('wp_enqueue_scripts', 'theme_name_scripts');
add_action('widgets_init', function () {

    register_widget('Integrated_Calendar_Widget');
});

//<div id="g_cal_upcoming_widget">
function convertToBoolean($myvar)
{
//return $myvar;


}

function CreateDataObject()
{


    $deprecated = null;
    $autoload = 'yes';
    add_option($GLOBALS["OPTION_DATA_ID"], [], $deprecated, $autoload);
}

function DoesDataObjectExcist()
{
    if (get_option($GLOBALS["OPTION_DATA_ID"]) !== false) {
        return true;

    } else {

        return false;
    }
}

function return_My_Option($key)
{

    $options = get_option($GLOBALS["OPTION_DATA_ID"]);
  return   $options[$key] ;
  //  update_option($GLOBALS["OPTION_DATA_ID"], $options);

}
function update_My_Option($key, $value)
{

    $options = get_option($GLOBALS["OPTION_DATA_ID"]);
    $options[$key] = $value;
    update_option($GLOBALS["OPTION_DATA_ID"], $options);

}

function setDefaults()
{
    //https://www.google.com/calendar/feeds/k835hjhh885m3s2h77mochjtcc%40group.calendar.google.com/public/full/n4mg73pebtdbp4182eum35reuk

    // update_My_Option("IntegratedCalendar_GoogleCalendar_Path","https://www.google.com/calendar/feeds/k835hjhh885m3s2h77mochjtcc%40group.calendar.google.com/public/full/n4mg73pebtdbp4182eum35reuk");
require_once( "Integrated_Calendar_Defaults.php");

}

function int_cal_init()
{

    if (!DoesDataObjectExcist()) {
        CreateDataObject();

    }

    setDefaults(); //// SET THE DEFAULT OPTIONS
    $GLOBALS['IntegratedCalendarOptions'] = get_option($GLOBALS["OPTION_DATA_ID"]);

    if (is_admin()) {

          require_once('Integrated_Calendar_Admin.php');
        //      require_once('test_page.php');
    } else {

        echo($GLOBALS["IntegratedCalendar_ShortCode_Enabled"]);
    }

}

/**
 * Proper way to enqueue scripts and styles
 */
function theme_name_scripts()
{

    //STYLES\  - will do this someday

    /*
    if (class_exists('WPLessPlugin')){



         $lessConfig = WPLessPlugin::getInstance()->getConfiguration();
        print_r($lessConfig);
        // compiles in the active theme, in a ‘compiled-css’ subfolder
        $lessConfig->setUploadDir(get_stylesheet_directory() . '/compiled-css');
       $lessConfig->setUploadUrl(get_stylesheet_directory_uri() . '/compiled-css');
    }else{

        echo "donesnt excists";

    }

   wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/calendar.less' );
*/






    wp_enqueue_style("IntegratedCalendar_style", $GLOBALS["PLUGIN_NAME_PATH"] . "/css/IntegratedCalendar.css"); //my plugin
    wp_enqueue_style("IntegratedCalendar_style_compiled", $GLOBALS["PLUGIN_NAME_PATH"] . "/css/integrated-calendar.css"); //my plugin


   //wp_enqueue_style("fullcalendar_style", $GLOBALS["PLUGIN_NAME_PATH"] . "/fullcalendar/fullcalendar.css");
   // wp_enqueue_style("css_modal", $GLOBALS["PLUGIN_NAME_PATH"] . "/css/reveal.css"); //modals
   // wp_enqueue_style("css_nano", $GLOBALS["PLUGIN_NAME_PATH"] . "/css/nano.css"); ///scroller


//SCRIPTS ( NOT MINE )
    //wp_enqueue_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js', array('jquery'), '1.8.16');
    wp_enqueue_script('jquery');
    wp_enqueue_script('moment', $GLOBALS["PLUGIN_NAME_PATH"] . '/fullcalendar/lib/moment.min.js', null, null, false); //dates for fullcalendar

   // wp_enqueue_script('qtip', $GLOBALS["PLUGIN_NAME_PATH"] . "/js/jquery.qtip-1.0.0/jquery.qtip-1.0.0-rc3.js", null, null, false);
   // wp_enqueue_script('nanoscroller', $GLOBALS["PLUGIN_NAME_PATH"] . '/js/jquery.nanoscroller.js', null, null, false); //scroller
 //   wp_enqueue_script('reveal', $GLOBALS["PLUGIN_NAME_PATH"] . '/js/jquery.reveal.js', null, null, false); //modals
    //  wp_enqueue_script('jquery_cookie', $GLOBALS["PLUGIN_NAME_PATH"] ."/js/jquery-cookie-master/jquery.cookie.js", null ,null, false);
   // wp_enqueue_script('bootstrap-tooltip', $GLOBALS["PLUGIN_NAME_PATH"] . '/js/bootstrap-tooltip.js', null, null, false); //modals



    wp_enqueue_script('fullcalendar', $GLOBALS["PLUGIN_NAME_PATH"] . '/fullcalendar/fullcalendar.js', null, null, true);
    wp_enqueue_script('gcal', $GLOBALS["PLUGIN_NAME_PATH"] . '/fullcalendar/gcal.js', null, null, true); /// THIS iS GOOGLE CALENDAR.

//

    ///INTEGRATED CALENDAR JS
    wp_enqueue_script('integratedcalendarview', $GLOBALS["PLUGIN_NAME_PATH"] . '/js/Integrated_Calendar_View.js', null, null, true);
    wp_enqueue_script('integratedcalendarobj', $GLOBALS["PLUGIN_NAME_PATH"] . '/js/Integrated_Calendar_Object.js', null, null, true);
    wp_enqueue_script('integratedcalendarbase', $GLOBALS["PLUGIN_NAME_PATH"] . '/js/Integrated_Calendar_Base.js', null, null,true);


   wp_register_script( 'jquery.magnific-popup.js', $GLOBALS["PLUGIN_NAME_PATH"]   . '/js/jquery.magnific-popup.js', array(), '1.0.0', false );
   wp_enqueue_script('jquery.magnific-popup.js');


    wp_register_script( 'g-mobile-util.js', $GLOBALS["PLUGIN_NAME_PATH"]   . '/js/g-mobile-util.js', array(), '1.0.0', false );
    wp_enqueue_script('g-mobile-util.js');
  // wp_enqueue_style( 'magnific-popup.css', get_template_directory_uri() .  '/css/magnific-popup.css' );


    if (class_exists('WPLessPlugin')) {
    } else {
    }

//    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/less/test.less' );
    /// wp_enqueue_style( 'responsive-nav', get_stylesheet_directory_uri() . '/js/responsive-nav.js-master/responsive-nav.css' );




    localizeScript();

}

function localizeScript()
{
    $GLOBALS['IntegratedCalendarOptions']["TEMPLATES"] = $GLOBALS["TEMPLATES"];
    $GLOBALS['IntegratedCalendarOptions']["SINGLE_EVENT_URL"] = $GLOBALS["SINGLE_EVENT_URL"];
    $GLOBALS['IntegratedCalendarOptions']["HOME_SITE"] = home_url();
  $GLOBALS['IntegratedCalendarOptions']["CURRENT_URI"]=$_SERVER["REQUEST_URI"];
    $GLOBALS['IntegratedCalendarOptions']["PLUGIN_NAME_PATH"]=$GLOBALS["PLUGIN_NAME_PATH"];




    wp_localize_script('integratedcalendarbase', 'IntegratedCalendar_Vars', $GLOBALS['IntegratedCalendarOptions']);
}

function my_shortcode_handler_intcal()
{


    return "<div id='integrated-calendar'></div>";
}

?>
