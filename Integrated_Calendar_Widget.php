<?php
/**
 * Created by JetBrains PhpStorm.
 * User: gbtunney
 * Date: 8/5/14
 * Time: 9:12 PM
 * To change this template use File | Settings | File Templates.
 */

class Integrated_Calendar_Widget extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        // widget actual processes
        parent::__construct(
            'Integrated_Calendar_Widget', // Base ID
            __('Integrated Calendar Widget', 'Integrated Calendar Widget'), // Name
            array( 'description' => __( 'Gillians integrated calendar', 'text_domain' ), ) // Args
        );

    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        // outputs the content of the widget

        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $args['before_widget'];
        if ( ! empty( $title ) ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        echo '<div id="integrated_calendar_widget"></div>';
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        // outputs the options form on admin
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'text_domain' );
        }?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    <p>Please change the settings in  Integrated_Calendar_Defaults.php . This options page api stuff is a serious piece of shit. I will change it soon.</p>
    </p><?php
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = array();
        $instance['title'] = strip_tags( $new_instance['title'] );

        return $instance;
    }
}

?>